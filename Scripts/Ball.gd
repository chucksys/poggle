extends RigidBody2D

signal ball_gone

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#func _physics_process(delta):
#	if applied_force.length() > 1:
#		applied_force *= 0.9
#	else:
#		applied_force = Vector2.ZERO

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
	emit_signal("ball_gone")

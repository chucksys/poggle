extends Node2D

# Declare member variables here. Examples:
onready var ball = load("res://Scenes/Objects/Ball.tscn")
var on_screen_balls = 0
const FIRE_FORCE = 300
const FIRE_TIME = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotation += get_local_mouse_position().angle() * 0.1

func fire(parent):
	if on_screen_balls > 0: return
	on_screen_balls += 1
	var b = ball.instance()
	b.apply_central_impulse((get_global_mouse_position() - position).normalized() * FIRE_FORCE * FIRE_TIME)
	b.position = position
	b.connect("ball_gone", self, "_on_ball_gone")
	parent.add_child(b)

func _on_ball_gone():
	if on_screen_balls > 0:
		on_screen_balls -= 1